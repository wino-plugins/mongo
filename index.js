const MongoDB = require('./MongoDB')

const configs = {
   has_connection_string: process.env.DATABASE_HAS_CONNECTION_STRING === 'true' ? true : false,
   connection_string: process.env.DATABASE_CONNECTION_STRING === 'true' ? true : false,
   has_auth: process.env.DATABASE_HAS_AUTH === 'true' ? true : false,
   has_direct_connection: process.env.DATABASE_HAS_DIRECT_CONNECTION === 'true' ? true : false,
   auth_mechanism: process.env.DATABASE_AUTH_MECHANISM || 'DEFAULT',
   has_replica_set: process.env.DATABASE_HAS_REPLICA_SETS  === 'true' ? true : false,
   has_retry_writes: process.env.DATABASE_HAS_RETRY_WRITES  === 'true' ? true : false,
   has_ssl: process.env.DATABASE_HAS_SSL  === 'true' ? true : false,
   ssl_key_dir: process.env.DATABASE_SSL_KEY_DIR || null,
   validate_ssl: process.env.DATABASE_VALIDATE_SSL  === 'true' ? true : false,
   ssl_cert_dir: process.env.DATABASE_SSL_CERT_DIR || null,
   ssl_ca_dir: process.env.DATABASE_SSL_CA_DIR || null,
   db: process.env.DATABASE_NAME || 'sample_db',
   user: process.env.DATABASE_USERNAME || 'sample',
   pass: process.env.DATABASE_PASSWORD || 'sample123!',
   host: process.env.DATABASE_HOST || '127.0.0.1',
   port: process.env.DATABASE_PORT || '27017',
   replica_set: process.env.DATABASE_REPLICA_SET || 'rs',
   auth_db: process.env.DATABASE_AUTHENTICATING_DB || 'admin',
}

async function init(){
   try {
      await MongoDB.connect()
   } catch (err) {
      throw err
   }
}

async function exit(){
   try {
      await MongoDB.disconnect()
   } catch (err) {
      throw err
   }
}

module.exports = {
   configs,
   init,
   exit
}