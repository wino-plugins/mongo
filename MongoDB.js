global.Mongoose = require('mongoose');
let dbOptions = {
   // autoIndex: false, // Don't build indexes
   useNewUrlParser: true,
   // useCreateIndex: true,
   // useFindAndModify: false,
   // reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
   // reconnectInterval: 500, // Reconnect every 500ms
   // poolSize: 10, // Maintain up to 10 socket connections
   // bufferMaxEntries: 0, // If not connected, return errors immediately rather than waiting for reconnect
   useUnifiedTopology: true, // Server Discovery and Monitoring engine
   connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
   socketTimeoutMS: 480000, // Close sockets after 45 seconds of inactivity
   // family: 4, // Use IPv4, skip trying IPv6
};
const fs = require('fs')
let dbConnection = null, isClosed = false;

function getMongoURL(){
   let connectionString = '';

   if (configs.mongo.has_auth) {
      connectionString = `mongodb://${configs.mongo.user}:${configs.mongo.pass}@${configs.mongo.host}:${configs.mongo.port}/${configs.mongo.db}?authMechanism=${configs.mongo.auth_mechanism}&authSource=${configs.mongo.auth_db}`;
   } else {
      connectionString = `mongodb://${configs.mongo.host}:${configs.mongo.port}/${configs.mongo.db}`;
   }

   // Configure direct connection


   connectionString = appendKey({string: connectionString, key: 'retryWrites', val: configs.mongo.has_retry_writes ? 'true': 'false'})
   connectionString = appendKey({string: connectionString, key: 'directConnection', val: configs.mongo.has_direct_connection ? 'true': 'false'})
   if(configs.mongo.has_replica_set){ connectionString = appendKey({string: connectionString, key: 'replicaSet', val: configs.mongo.replica_set}) }
   if(configs.mongo.has_ssl){ 
      connectionString = appendKey({string: connectionString, key: 'ssl', val: 'true'}) 

      dbOptions = {...dbOptions, ...{
         ssl: true,
         sslValidate: configs.mongo.validate_ssl,
         sslCA: fs.existsSync(configs.mongo.ssl_key_dir) ? configs.mongo.ssl_key_dir : null,
         // sslKey: fs.existsSync(configs.mongo.ssl_key_dir) ? configs.mongo.ssl_key_dir : null,
         // sslCert: fs.existsSync(configs.mongo.ssl_cert_dir) ? configs.mongo.ssl_cert_dir : null,
         // sslCA: fs.existsSync(configs.mongo.ssl_ca_dir) ? configs.mongo.ssl_ca_dir : null,
      }}

   }

   if(configs.mongo.has_connection_string){
      connectionString = configs.mongo.connection_string
   }
   return connectionString;
}

function appendKey({string,key,val}){
   try {
      let operator = '?'

      if(string.includes('?')){
         operator = `&`
      }

      string += `${operator}${key}=${val}`

      return string
   } catch (err) {
      throw err
   }
}

function connect(){
   return new Promise(async (resolve,reject)=> {
      try {
         let connectionURL = getMongoURL();

         // dbConnection = await connectToDB(connectionURL)
         dbConnection = await connectToDBAsync(connectionURL)
         resolve(true)
      } catch (err) {reject(err);}
   })
}

function connectToDB(connectionURL){
   return new Promise((resolve,reject) => {
      try {
         Mongoose.connect(connectionURL, dbOptions, async (err, db) => {
            if (err) {
               console.error(chalk.red(`❌ Error connecting to MongoDB: ${connectionURL}`));
               reject(err);
            } else {
               console.log(chalk.cyan(`✔️ Successfully connected to MongoDB`));
               resolve(db)
            }
         })
      } catch (err) {
         reject(err)
      }
   })
}

async function connectToDBAsync(connectionURL){
   try {
      let db = await Mongoose.connect(connectionURL, dbOptions)
      console.log(chalk.cyan(`✔️ Successfully connected to MongoDB: ${connectionURL}`));
      return db
   } catch (err) {
      console.error(chalk.red(`❌ Error connecting to MongoDB: ${connectionURL}`));
      throw err
   }
}

function createConnection(connectionURL, name){
   return new Promise((resolve,reject) => {
      try {
         Mongoose.createConnection(connectionURL, dbOptions, async (err, db) => {
            if (err) {
               console.error(chalk.red(`❌ Error connecting to ${name || 'Direct'} DB: ${connectionURL}`));
               reject(err);
            } else {
               console.log(chalk.cyan(`✔️ Successfully connected to ${name || 'Direct'} DB`));
               resolve(db)
            }
         })
      } catch (err) {
         reject(err)
      }
   })
}


function disconnect(){
   if(!isClosed && dbConnection != null){
      Mongoose.connection.close()
      isClosed = true
      console.log('[X] MongoDB connection closed.')
   }
}

module.exports = {
   connect,
   getMongoURL,
   disconnect,
   createConnection,
}