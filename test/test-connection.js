require('dotenv').config();
let app = require('../index')
global.rootDir = __dirname
global.configs = { mongo: app.configs}
global.chalk = require('chalk')

app.init()